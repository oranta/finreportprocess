﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.BL.Interfaces
{
    public interface IBranchZonesService
    {
        Task<IEnumerable<BranchZone>> GetAllBranchZonesAsync();
        int? GetZoneByBranchCode(string branchCode);

        Dictionary<string, string> BranchZoneAdapterDictionary { get; set; }
        int? ChangeZone(string branchCode, int? zoneId);
    }
}