﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Interfaces
{
    public interface IBranchZoneRepository
    {
        Task<IEnumerable<BranchZone>> GetBranchZonesAsync();
        Task<Dictionary<string, string>> GetBranchZoneAdapterDictionary();
    }
}