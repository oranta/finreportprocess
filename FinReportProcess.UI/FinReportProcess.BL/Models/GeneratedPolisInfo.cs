﻿using System;

namespace FinReportProcess.BL.Models
{
    public class GeneratedPolisInfo
    {
        public string AgentName { get; set; }
        public string AgentFaceId { get; set; }
        public int BusinessLineId { get; set; }
        public string ProgramCode { get; set; }
        public DateTime PolisRegisteredDate { get; set; }
        public string PolisNumber { get; set; }
        public string TariffName { get; set; }
        public string InsuredName { get; set; }
        public decimal PolisRealPayments { get; set; }
        public decimal Comission { get; set; }
        public decimal ComissionValue { get; set; }
        public string ActStatus { get; set; }
        public string BranchCode { get; set; }
        public string DirectionCode { get; set; }
        public int? BranchZone { get; set; }
        public string PersonType { get; set; }
        public string ProgramTypeName { get; set; }
        public int ChannelCode { get; set; }
        public decimal? IcsValue { get; set; }
        public decimal? Ics { get; set; }
        public string AgentIcsName { get; set; }
        public string AgentIcsChanelName { get; set; }
        public int? AgentIcsChanelCode { get; set; }
        public int? Franchise { get; set; }
        public string PolisTeritoryCode { get; set; }
        public decimal? PolisCoefficientK9 { get; set; }
        public decimal? PolisCoefficientK8 { get; set; }
        public int PolisDurationInDays { get; set; }
        public string VehicleType { get; set; }
        public double? AgentComissionPermissions { get; set; }
        public double? IcsComissionPermissions { get; set; }

        public double? IcsControlledValue { get; set; }
        public string Comments { get; set; }

        public double? IcsQuota { get; set; }

        public string Parking { get; set; }

        public override string ToString()
        {
            return $"{AgentName}\t{AgentFaceId}\t{BusinessLineId}\t{ProgramCode}\t" +
                   $"{PolisRegisteredDate}\t{PolisNumber}\t{InsuredName}\t" +
                   $"{PolisRealPayments}\t{Comission}\t{ComissionValue}\t{ActStatus}\t" +
                   $"{BranchCode}\t{DirectionCode}\t{BranchZone}\t{PersonType}\t" +
                   $"{ProgramTypeName}\t{ChannelCode}\t{IcsValue}\t{AgentIcsName}\t" +
                   $"{Franchise}\t{PolisTeritoryCode}\t{PolisCoefficientK9}\t" +
                   $"{PolisCoefficientK8}\t{PolisDurationInDays}\t{AgentComissionPermissions}\t" +
                   $"{IcsComissionPermissions}\t{IcsControlledValue}\t{Comments}\t{IcsQuota}\t{Parking}\t";
        }
    }
}