﻿using System.Collections;

namespace FinReportProcess.DAL.Models
{
    public class PolisQuotaIcs
    {
        public string PolisNumber { get; set; }
        public double IcsQuota { get; set; }

    }
}
