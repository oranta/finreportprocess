﻿namespace FinReportProcess.DAL.Models
{
    public class ComissionPermissions
    {
        public string PolisNumber { get; set; }
        public double AgentComissionPermissions { get; set; }
        public double IcsComissionPermissions { get; set; }
    }
}