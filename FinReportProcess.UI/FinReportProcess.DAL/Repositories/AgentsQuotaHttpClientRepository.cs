﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using ExcelDataReader;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Repositories
{
    public class AgentsQuotaHttpClientRepository : IAgentsQuotaRepository
    {
        private readonly HttpClient _client;


        public AgentsQuotaHttpClientRepository()
        {
            var clientHandler = new HttpClientHandler { Credentials = PortalSettings.ServiceCredential };
            _client = new HttpClient(clientHandler);
        }

        public async Task<DataTable> ProcessFileFromUrlAsync(string targetUrl)
        {
            HttpResponseMessage response = await _client.GetAsync(targetUrl);
            
            response.EnsureSuccessStatusCode();
            Stream responseFileStream = await response.Content.ReadAsStreamAsync();

            return CreateDataTableFromStream(responseFileStream);
        }

        private static DataTable CreateDataTableFromStream(Stream ms)
        {
            var excelDataReader = ExcelReaderFactory.CreateReader(ms);

            var result = excelDataReader.AsDataSet(new ExcelDataSetConfiguration
            {
                ConfigureDataTable = (table) => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true,
                    ReadHeaderRow = (rowReader) => { rowReader.Read(); }
                }

            });

            return result.Tables[0];
        }


        private static string GetTargetFileName(string filePrefix, DateTime period)
        {
            return $"{filePrefix}_{period.Month:00}-{period.Year}.xlsx";
        }

        public async Task<IEnumerable<ComissionPermissions>> GetComissionPermissionsesAsync(DateTime period)
        {
            // Grant ex.: new ComissionPermissions { PolisNumber = "АО-5181797", IcsComissionPermissions = 0, AgentComissionPermissions = 35 }

            string permissionUrl = string.Format(PortalSettings.GrantFilePathUrl, GetTargetFileName(PortalSettings.GrantFileNamePrefix, period));

            Console.WriteLine($"Get data from {permissionUrl}");

            DataTable permissionDataTable = await ProcessFileFromUrlAsync(permissionUrl);


            var result = permissionDataTable.AsEnumerable().Select(x => new ComissionPermissions
            {
                PolisNumber = x[0].ToString(),
                IcsComissionPermissions = x.Field<double>(1),
                AgentComissionPermissions = x.Field<double>(2)
            });

            return result;
        }


        public async Task<IEnumerable<ControlledPolis>> GetControlledPolisesAsync(DateTime period)
        {
            // Control ex.: new ControlledPolis { PolisNumber = "АО-5181797", IcsControlledValue = 5, Comments = "партнер, квота, січень 2019" }

            string controlUrl = string.Format(PortalSettings.ControlFilePathUrl, GetTargetFileName(PortalSettings.ControlFileNamePrefix, period));

            Console.WriteLine($"Get data from {controlUrl}");

            DataTable permissionDataTable = await ProcessFileFromUrlAsync(controlUrl);


            var result = permissionDataTable.AsEnumerable().Select(x => new ControlledPolis
            {
                PolisNumber = x[0].ToString(),
                IcsControlledValue = x.Field<double>(1),
                Comments = x.Field<string>(2)
            });

            return result;
        }


        public async Task<IEnumerable<ParkingPolis>> GeQuotaPolisesAsync(DateTime period)
        {
            // Quota ex.: new ParkingPolis { PolisNumber = "АО-5181797", Parking = "ні" }

            string quotaUrl = string.Format(PortalSettings.QuotaFilePathUrl, GetTargetFileName(PortalSettings.QuotaFileNamePrefix, period));

            Console.WriteLine($"Get data from {quotaUrl}");

            DataTable permissionDataTable = await ProcessFileFromUrlAsync(quotaUrl);

            var result = permissionDataTable.AsEnumerable().Select(x => new ParkingPolis
            {
                PolisNumber = x[0].ToString(),
                Parking = x.Field<string>(1)
            });

            return result;

        }


        public async Task<IEnumerable<PolisQuotaIcs>> GetQuotaIcsesAsync(DateTime period)
        {
            // new PolisQuotaIcs { PolisNumber = "АО-5181797", IcsQuota = 13 }
            string newUrl = string.Format(PortalSettings.NewFilePathUrl, GetTargetFileName(PortalSettings.NewFileNamePrefix, period));

            Console.WriteLine($"Get data from {newUrl}");

            DataTable permissionDataTable = await ProcessFileFromUrlAsync(newUrl);

            var result = permissionDataTable.AsEnumerable().Select(x => new PolisQuotaIcs
            {
                PolisNumber = x[0].ToString(),
                IcsQuota = x.Field<double>(1)
            });

            return result;
        }



    }
}