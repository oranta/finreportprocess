import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const headers = new HttpHeaders(
  {
    'Authorization': 'Basic ' + btoa(`SPConfigAcct:SPConfigAcct`)
  });

@Injectable()
export class ConfigService {
  baseUrl = "http://portal.or.nt/Finance/NEW/NEW_06-2019.xlsx"

  constructor(private http: HttpClient) { }


  get() {
    this.http.get(this.baseUrl, { headers })
      .subscribe(
        data => console.log(data),
        error => console.log(error)
      );
  }
}
