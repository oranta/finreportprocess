﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Interfaces
{
    public interface IRepositoryAgentsComission
    {
        Task<IEnumerable<AgentComission>> GetAgentComissions(DateTime period);
    }
}