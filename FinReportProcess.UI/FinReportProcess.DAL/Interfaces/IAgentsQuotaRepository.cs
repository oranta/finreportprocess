﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Interfaces
{
    public interface IAgentsQuotaRepository
    {
        Task<IEnumerable<ParkingPolis>> GeQuotaPolisesAsync(DateTime period);
        Task<IEnumerable<ComissionPermissions>> GetComissionPermissionsesAsync(DateTime period);
        Task<IEnumerable<ControlledPolis>> GetControlledPolisesAsync(DateTime period);
        Task<IEnumerable<PolisQuotaIcs>> GetQuotaIcsesAsync(DateTime period);
    }
}