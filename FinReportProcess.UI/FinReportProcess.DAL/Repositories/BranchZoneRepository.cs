﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Repositories
{
    public class BranchZoneRepository : IBranchZoneRepository
    {
        private readonly SqlConnection _db05Connection;

        public readonly Dictionary<string, string> BranchZoneAdapterDictionary = new Dictionary<string, string>
        {
            {"1001", "3302"},
            {"1002", "3303"},
            {"1003", "3319"},
            {"1005", "3315"},
            {"1006", "3301"},
            {"1007", "3302"},
            {"1008", "3305"},
            {"1009", "3306"},
            {"1010", "3307"},
            {"1011", "3304"},
            {"1012", "3308"},
            {"1013", "3309"},
            {"1014", "3310"},
            {"1015", "3317"},
            {"1016", "3318"},
            {"1017", "3320"},
            {"1018", "3321"},
            {"1019", "3330"},
            {"1020", "3331"},
            {"1021", "3333"},
            {"1022", "3334"},
            {"1023", "3345"},
            {"1024", "3337"},
            {"1025", "3339"},
            {"1026", "3341"},
            {"1027", "3343"},
            {"1028", "3344"},
            {"1029", "3346"},
            {"1030", "3349"},
            {"1031", "3346"},
            {"1032", "3311"},
            {"1090", "3390"},
            {"2601", "3314"},
            {"2602", "3313"},
            {"2604", "3340"},
            {"2605", "3316"},
            {"2606", "3338"},
            {"2607", "3342"},
            {"2608", "3332"},
            {"2609", "3312"},
            {"2610", "3335"},
            {"2611", "3336"},
            {"2613", "3347"},
            {"2614", "3348"},
            {"2615", "3322"},
            {"2616", "3323"},
            {"2617", "3324"},
            {"2619", "3326"},
            {"2620", "3327"},
            {"2621", "3325"},
            {"2622", "3328"},
            {"2623", "3329"},
            {"2624", "3327"},
            {"2625", "3323"},
            {"2690", "3390"}
        };

        public BranchZoneRepository()
        {
            _db05Connection = new SqlConnection("Data Source=hq01db02;Initial Catalog=ServiceBase;Integrated Security=True;Connect Timeout=0;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        }

        public async Task<IEnumerable<BranchZone>> GetBranchZonesAsync()
        {
            var branchZonesDataTable = await GetBranchZonesTable();
            List<BranchZone> branchZones = new List<BranchZone>();

            branchZones = (from DataRow dr in branchZonesDataTable.Rows
                select new BranchZone()
                {
                    BranchCode = dr.Field<string>("BranchCode"),
                    BranchZoneId = dr.Field<int>("BranchZone")
                }).ToList();
            return branchZones;
        }

        public async Task<Dictionary<string, string>> GetBranchZoneAdapterDictionary()
        {
            return await Task.Run(() => BranchZoneAdapterDictionary);
        }

        private SqlCommand CreateBranchZonesCommand()
        {
            string agentComissionQuery = $"SELECT B.BranchCode, BZ.BranchZone\r\nFROM OrantaSch.dbo.Branches AS B\r\nINNER JOIN ServiceBase.dbo.BranchesZone AS BZ ON B.gid = BZ.gid\r\nORDER BY B.BranchCode";

            SqlCommand command = new SqlCommand(agentComissionQuery);
            command.CommandTimeout = 0;

            return command;
        }
        private async Task<DataTable> GetBranchZonesTable()
        {
            DataSet ds = new DataSet();

            SqlCommand branchZonesCommand = CreateBranchZonesCommand();
            branchZonesCommand.Connection = _db05Connection;

            SqlDataAdapter adapter = new SqlDataAdapter(branchZonesCommand);

            Console.WriteLine($"Get branchZones from SQL");


            try
            {
                await Task.Run(() => adapter.Fill(ds, "BranchZones"));
            }
            catch (SqlException sqlException)
            {
                Console.WriteLine(sqlException.Message);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }


            return ds.Tables["BranchZones"];
        }


    }
}