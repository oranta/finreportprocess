﻿namespace FinReportProcess.DAL.Models
{
    public class ControlledPolis
    {
        public string PolisNumber { get; set; }
        public double IcsControlledValue { get; set; }
        public string Comments { get; set; }
    }
}