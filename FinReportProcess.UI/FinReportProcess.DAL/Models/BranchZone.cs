﻿
namespace FinReportProcess.DAL.Models
{
    public class BranchZone
    {
        public string BranchCode { get; set; }
        public int BranchZoneId { get; set; }
    }
}
