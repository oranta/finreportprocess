﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.BL.Interfaces;
using FinReportProcess.DAL;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.BL.Services
{
    public class AgentsQuotaService : IAgentsQuotaService
    {
        private readonly IAgentsQuotaRepository _agentsQuotaRepository;

        public AgentsQuotaService(IAgentsQuotaRepository agentsQuotaRepository)
        {
            _agentsQuotaRepository = agentsQuotaRepository;
        }

        public async Task<IEnumerable<ComissionPermissions>> GetComissionPermissionsesAsync(DateTime period)
        {
            // todo: Grant
            return await _agentsQuotaRepository.GetComissionPermissionsesAsync(period);

        }

        public async Task<IEnumerable<ControlledPolis>> GetControlledPolisesAsync(DateTime period)
        {
            // todo: Control
            return await  _agentsQuotaRepository.GetControlledPolisesAsync(period);
        }

        public async Task<IEnumerable<ParkingPolis>> GeQuotaPolisesAsync(DateTime period)
        {
            // todo: Quota
            return await _agentsQuotaRepository.GeQuotaPolisesAsync(period);

        }

        public async Task<IEnumerable<PolisQuotaIcs>> GetQuotaIcsesAsync(DateTime period)
        {
            return await _agentsQuotaRepository.GetQuotaIcsesAsync(period);
        }
    }
}