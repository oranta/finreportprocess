﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinReportProcess.WEB.Models
{
    public class PortalFile
    {
        public string Name { get; set; }
        public bool MyProperty { get; set; }
    }
}
