﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FinReportProcess.BL;
using FinReportProcess.BL.Models;
using FinReportProcess.BL.Services;
using FinReportProcess.DAL.Repositories;
using OfficeOpenXml;

namespace FinReportProcess.UI
{
    class Program
    {
        

        static void Main(string[] args)
        {
            //DateTime period = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 1);

            DateTime period = DateTime.Now.AddMonths(-1);
            period = new DateTime(period.Year, period.Month, 1);
            var path = Path.Combine(Environment.CurrentDirectory, $"Report_{period.Month:00}-{period.Year}.xlsx");


            var watch = Stopwatch.StartNew();
            

            var test = ReportProcess.Init(
                period,
                new AgentActComissionService(new RepositoryAgentsComission()), 
                new AgentsQuotaService(new AgentsQuotaHttpClientRepository()),
                new BranchZonesService(new BranchZoneRepository()), true);



            var report = test.Result.ProcessReport(period);


            List<GeneratedPolisInfo> generatedPolisInfos = report.ToList();
            var count = generatedPolisInfos.Count();

            watch.Stop();
            var elapsed = watch.ElapsedMilliseconds;

            Console.WriteLine($"Выполнение запроса заняло {elapsed}");


            FileInfo reportFileInfo = new FileInfo(path);

            if(reportFileInfo.Exists) reportFileInfo.Delete();

            Console.WriteLine("Создание ексель отчета");

            using (ExcelPackage reportPackage = new ExcelPackage(reportFileInfo))
            {
                ExcelWorksheet reporpExcelWorksheet = reportPackage.Workbook.Worksheets.Add("Report");
                reporpExcelWorksheet.Cells[1, 1].Value = "Aгент АВ";
                reporpExcelWorksheet.Cells[1, 2].Value = "ІНН або ЄДРПОУ агента";
                reporpExcelWorksheet.Cells[1, 3].Value = "Лінія бізнесу";
                reporpExcelWorksheet.Cells[1, 4].Value = "Код виду страхування";
                reporpExcelWorksheet.Cells[1, 5].Value = "Дата укладання";
                reporpExcelWorksheet.Cells[1, 6].Value = "Поліс";
                reporpExcelWorksheet.Cells[1, 7].Value = "Тариф";
                reporpExcelWorksheet.Cells[1, 8].Value = "Страхувальник";
                reporpExcelWorksheet.Cells[1, 9].Value = "Фактичний платіж";
                reporpExcelWorksheet.Cells[1, 10].Value = "АВ, грн";
                reporpExcelWorksheet.Cells[1, 11].Value = "АВ, %";
                reporpExcelWorksheet.Cells[1, 12].Value = "Статус акту";
                reporpExcelWorksheet.Cells[1, 13].Value = "Код відділення";
                reporpExcelWorksheet.Cells[1, 14].Value = "Код дирекції";
                reporpExcelWorksheet.Cells[1, 15].Value = "Зона відділення";
                reporpExcelWorksheet.Cells[1, 16].Value = "Тип особи(агента АВ)";
                reporpExcelWorksheet.Cells[1, 17].Value = "Тип виду страхування";
                reporpExcelWorksheet.Cells[1, 18].Value = "Код каналу";
                reporpExcelWorksheet.Cells[1, 19].Value = "ІКП, %";
                reporpExcelWorksheet.Cells[1, 20].Value = "ІКП,грн";
                reporpExcelWorksheet.Cells[1, 21].Value = "Агент ІКП";
                reporpExcelWorksheet.Cells[1, 22].Value = "Канал агента ІКП";
                reporpExcelWorksheet.Cells[1, 23].Value = "Код канала агента ІКП";
                reporpExcelWorksheet.Cells[1, 24].Value = "Франшиза";
                reporpExcelWorksheet.Cells[1, 25].Value = "Територія дії договору";
                reporpExcelWorksheet.Cells[1, 26].Value = "Знижка, К9";
                reporpExcelWorksheet.Cells[1, 27].Value = "Бонус-малус, К8";
                reporpExcelWorksheet.Cells[1, 28].Value = "Термін дії полісу";
                reporpExcelWorksheet.Cells[1, 29].Value = "Тип транспортного засобу";
                reporpExcelWorksheet.Cells[1, 30].Value = "Дозвіл АВ, % (GRANT)";
                reporpExcelWorksheet.Cells[1, 31].Value = "Дозвіл ІКП, % (GRANT)";
                reporpExcelWorksheet.Cells[1, 32].Value = "ІКП % (CONTROL)";
                reporpExcelWorksheet.Cells[1, 33].Value = "ІКП примітка (CONTROL)";
                reporpExcelWorksheet.Cells[1, 34].Value = "ІКП квота, (NEW)";
                reporpExcelWorksheet.Cells[1, 35].Value = "Парк, (QUOTA)";

                reporpExcelWorksheet.Cells[2, 1].LoadFromCollection(generatedPolisInfos);

                reportPackage.Save();
            }

             Console.ReadKey();

        }

    }
}
