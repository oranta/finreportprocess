﻿namespace FinReportProcess.DAL.Models
{
    public class ParkingPolis
    {
        public string PolisNumber { get; set; }
        public string Parking { get; set; }
    }
}