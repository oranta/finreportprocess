import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portal-files',
  templateUrl: './portal-files.component.html',
  styleUrls: ['./portal-files.component.scss']
})
export class PortalFilesComponent implements OnInit {

  // todo: get files isAvailable from request
  filesInfo = [
    { title: "GRANT", name: "GRANT_07-2019.xlsx", isAvailable: true },
    { title: "NEW", name: "NEW_07-2019.xlsx", isAvailable: false },
    { title: "CONTROL", name: "CONTROL_07-2019.xlsx", isAvailable: true },
    { title: "QUOTA", name: "QUOTA_07-2019.xlsx", isAvailable: false },
  ]

  constructor() { }

  ngOnInit() {
  }

}
