﻿using System.Net;

namespace FinReportProcess.DAL
{
    public static class PortalSettings
    {
        public static NetworkCredential ServiceCredential { get; set; } =
            new NetworkCredential("SPConfigAcct", "SPConfigAcct", "ORANTA");
        public static string BaseUrl { get; set; } = "http://portal.or.nt/Finance/";
        public static string NewFilePathUrl { get; set; } = "http://portal.or.nt/Finance/NEW/{0}";
        public static string QuotaFilePathUrl { get; set; } = "http://portal.or.nt/Finance/QUOTA/{0}";
        public static string GrantFilePathUrl { get; set; } = "http://portal.or.nt/Finance/GRANT/{0}";
        public static string ControlFilePathUrl { get; set; } = "http://portal.or.nt/Finance/CONTROL/{0}";

        public static string NewFileNamePrefix { get; set; } = "NEW";
        public static string QuotaFileNamePrefix { get; set; } = "QUOTA";
        public static string GrantFileNamePrefix { get; set; } = "GRANT";
        public static string ControlFileNamePrefix { get; set; } = "CONTROL";
    }
}