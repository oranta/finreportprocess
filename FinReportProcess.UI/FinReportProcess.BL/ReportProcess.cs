﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinReportProcess.BL.Interfaces;
using FinReportProcess.BL.Models;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.BL
{
    public sealed class ReportProcess
    {
        private static readonly ReportProcess Instance = new ReportProcess();

        private static IAgentActComissionService AgentActComissionService { get; set; }

        private static IAgentsQuotaService AgentsQuotaService { get; set; }

        public static IBranchZonesService BranchZonesService { get; set; }

        public IEnumerable<ParkingPolis> QuotaPolises { get; private set; }
        public IEnumerable<PolisQuotaIcs> QuotaIcses { get; private set; }
        public IEnumerable<ControlledPolis> ControlledPolises { get; private set; }
        public IEnumerable<ComissionPermissions> ComissionPermissionses { get; private set; }
        public IEnumerable<AgentComission> AgentComissions { get; private set; }

        public IEnumerable<BranchZone> BranchZones { get; private set; }

        public IEnumerable<GeneratedPolisInfo> ResultReport { get; set; }

        public static bool IsAgentComissionOnly { get; set; }

        static ReportProcess() { }
        private ReportProcess() { }

        public static async Task<ReportProcess> Init(DateTime period, IAgentActComissionService agentActComissionService, IAgentsQuotaService agentsQuotaService, IBranchZonesService branchZonesService, bool isJupiterReportOnly = false)
        {
            IsAgentComissionOnly = isJupiterReportOnly;
            if (!IsAgentComissionOnly)
            {
                AgentsQuotaService = agentsQuotaService;
            }

            AgentActComissionService = agentActComissionService;
            BranchZonesService = branchZonesService;

            await Instance.LoadDataAsync(period);

            return Instance;
        }


        public async Task LoadDataAsync(DateTime period)
        {
            if (!IsAgentComissionOnly)
            {
                await LoadFromJupiterAndPortal(period);
            }
            else
            {
                await LoadAgentComission(period);
            }

        }

        private async Task LoadAgentComission(DateTime period)
        {
            Task t5 = Task.Run(() => BranchZones = BranchZonesService.GetAllBranchZonesAsync().Result);
            Task t6 = Task.Run(() => AgentComissions = AgentActComissionService.GetAgentComissionsAsync(period).Result);

            await Task.WhenAll(new[] { t5, t6 });
        }

        private async Task LoadFromJupiterAndPortal(DateTime period)
        {
            Task t1 = Task.Run(() => ComissionPermissionses = AgentsQuotaService.GetComissionPermissionsesAsync(period).Result);
            Task t2 = Task.Run(() => ControlledPolises = AgentsQuotaService.GetControlledPolisesAsync(period).Result);
            Task t3 = Task.Run(() => QuotaIcses = AgentsQuotaService.GetQuotaIcsesAsync(period).Result);
            Task t4 = Task.Run(() => QuotaPolises = AgentsQuotaService.GeQuotaPolisesAsync(period).Result);

            Task t5 = Task.Run(() => BranchZones = BranchZonesService.GetAllBranchZonesAsync().Result);
            Task t6 = Task.Run(() => AgentComissions = AgentActComissionService.GetAgentComissionsAsync(period).Result);

            await Task.WhenAll(new[] { t1, t2, t3, t4, t5, t6 });
        }

        public IEnumerable<GeneratedPolisInfo> ProcessReport(DateTime period)
        {
            Console.WriteLine("Получение отчета");
            IEnumerable<GeneratedPolisInfo> rawDataAfterJoin = GetReport();

            Console.WriteLine("Замена платежей");
            IEnumerable<GeneratedPolisInfo> changedRealPayments = ChangeRealPaymentsFor17Chanel(rawDataAfterJoin);

            Console.WriteLine("Заполнение зоны");
            IEnumerable<GeneratedPolisInfo> filledBranchZones = FillEmptyBranchZones(changedRealPayments);

            Console.WriteLine("Замена типа физ и юр лиц");
            IEnumerable<GeneratedPolisInfo> changePersonsType = ReplacePersonsType(filledBranchZones);

            Console.WriteLine("Замена зон для КРД и Киевской дирекции");
            IEnumerable<GeneratedPolisInfo> result = ChangeBranchZones(changePersonsType);

            return result.ToList();
        }

        private IEnumerable<GeneratedPolisInfo> ChangeBranchZones(IEnumerable<GeneratedPolisInfo> generatedPolisCollection)
        {
            return generatedPolisCollection.Select(p =>
            {
                p.BranchZone = BranchZonesService.ChangeZone(p.BranchCode, p.BranchZone);
                return p;
            });
        }

        private IEnumerable<GeneratedPolisInfo> ReplacePersonsType(IEnumerable<GeneratedPolisInfo> generatedPolisCollection)
        {
            return generatedPolisCollection.Select(p =>
            {
                if (p.ChannelCode == 11 || p.ChannelCode == 12 || p.ChannelCode == 21)
                {
                    p.PersonType = "Ф";
                }
                else
                {
                    p.PersonType = "Ю";
                }

                return p;
            });
        }

        private static IEnumerable<GeneratedPolisInfo> FillEmptyBranchZones(IEnumerable<GeneratedPolisInfo> generatedPolisCollection)
        {
            return generatedPolisCollection.Select(p =>
            {
                if (p.BranchZone == null)
                    p.BranchZone = BranchZonesService.GetZoneByBranchCode(p.BranchCode);
                return p;
            });
        }

        private static IEnumerable<GeneratedPolisInfo> ChangeRealPaymentsFor17Chanel(IEnumerable<GeneratedPolisInfo> generatedPolisCollection)
        {
            return generatedPolisCollection.Select(p =>
            {
                if (p.ProgramCode == "182" && p.ChannelCode == 17 && p.ComissionValue == 1)
                    p.PolisRealPayments = 0;
                return p;
            });
        }

        private IEnumerable<GeneratedPolisInfo> GetReport()
        {
            return !IsAgentComissionOnly ? GetJoinedDataList() : GetAgentComissionList();
        }

        private IEnumerable<GeneratedPolisInfo> GetAgentComissionList()
        {
            return AgentComissions.Select(ac => new GeneratedPolisInfo()
            {
                AgentName = ac.AgentName,
                AgentFaceId = ac.AgentFaceId,
                BusinessLineId = ac.BusinessLineId,
                ProgramCode = ac.ProgramCode,
                PolisRegisteredDate = ac.PolisRegisteredDate,
                PolisNumber = ac.PolisNumber,
                TariffName = ac.TariffName,
                InsuredName = ac.InsuredName,
                PolisRealPayments = ac.PolisRealPayments,
                Comission = ac.Comission,
                ComissionValue = ac.ComissionValue,
                ActStatus = ac.ActStatus,
                BranchCode = ac.BranchCode,
                DirectionCode = ac.DirectionCode,
                BranchZone = ac.BranchZone,
                PersonType = ac.PersonType,
                ProgramTypeName = ac.ProgramTypeName,
                ChannelCode = ac.ChannelCode,
                IcsValue = ac.IcsValue,
                Ics = ac.Ics,
                AgentIcsName = ac.AgentIcsName,
                Franchise = ac.Franchise,
                PolisTeritoryCode = ac.PolisTeritoryCode,
                PolisCoefficientK8 = ac.PolisCoefficientK8,
                PolisCoefficientK9 = ac.PolisCoefficientK9,
                PolisDurationInDays = ac.PolisDurationInDays,
                VehicleType = ac.VehicleType,
                AgentIcsChanelCode = ac.AgentIcsChanelCode,
                AgentIcsChanelName = ac.AgentIcsChanelName
            });
        }

        private IEnumerable<GeneratedPolisInfo> GetJoinedDataList()
        {
            return AgentComissions
            .GroupJoin(QuotaPolises, ac => ac.PolisNumber, qp => qp.PolisNumber, (ac, qp) => new GeneratedPolisInfo
            {
                AgentName = ac.AgentName,
                AgentFaceId = ac.AgentFaceId,
                BusinessLineId = ac.BusinessLineId,
                ProgramCode = ac.ProgramCode,
                PolisRegisteredDate = ac.PolisRegisteredDate,
                PolisNumber = ac.PolisNumber,
                TariffName = ac.TariffName,
                InsuredName = ac.InsuredName,
                PolisRealPayments = ac.PolisRealPayments,
                Comission = ac.Comission,
                ComissionValue = ac.ComissionValue,
                ActStatus = ac.ActStatus,
                BranchCode = ac.BranchCode,
                DirectionCode = ac.DirectionCode,
                BranchZone = ac.BranchZone,
                PersonType = ac.PersonType,
                ProgramTypeName = ac.ProgramTypeName,
                ChannelCode = ac.ChannelCode,
                IcsValue = ac.IcsValue,
                Ics = ac.Ics,
                AgentIcsName = ac.AgentIcsName,
                Franchise = ac.Franchise,
                PolisTeritoryCode = ac.PolisTeritoryCode,
                PolisCoefficientK8 = ac.PolisCoefficientK8,
                PolisCoefficientK9 = ac.PolisCoefficientK9,
                PolisDurationInDays = ac.PolisDurationInDays,
                VehicleType = ac.VehicleType,
                AgentIcsChanelCode = ac.AgentIcsChanelCode,
                AgentIcsChanelName = ac.AgentIcsChanelName,
                Parking = qp.SingleOrDefault()?.Parking
            })
            .GroupJoin(QuotaIcses, gp => gp.PolisNumber, qp => qp.PolisNumber, (gp, qp) => new { gp, qp })
            .SelectMany(
                temp => temp.qp.DefaultIfEmpty(),
                (tgp, tqp) =>
                {
                    tgp.gp.IcsQuota = tqp?.IcsQuota;
                    return tgp.gp;
                })
            .GroupJoin(ControlledPolises, gp => gp.PolisNumber, cp => cp.PolisNumber,
                (gp, cp) => new { gp, cp })
            .SelectMany(
                temp => temp.cp.DefaultIfEmpty(),
                (tgp, tcp) =>
                {
                    tgp.gp.IcsControlledValue = tcp?.IcsControlledValue;
                    tgp.gp.Comments = tcp?.Comments;
                    return tgp.gp;
                })
            .GroupJoin(ComissionPermissionses, gp => gp.PolisNumber, cpr => cpr.PolisNumber, (gp, cpr) => new { gp, cpr })
            .SelectMany(
                temp => temp.cpr.DefaultIfEmpty(),
                (x, y) =>
                {
                    x.gp.AgentComissionPermissions = y?.AgentComissionPermissions;
                    x.gp.IcsComissionPermissions = y?.IcsComissionPermissions;
                    return x.gp;
                }).ToList();
        }
    }
}