﻿using System;

namespace FinReportProcess.DAL.Models
{
    public class AgentComission
    {
        public string AgentName { get; set; }
        public string AgentFaceId { get; set; }
        public int BusinessLineId { get; set; }
        public string ProgramCode { get; set; }
        public DateTime PolisRegisteredDate { get; set; }
        public string PolisNumber { get; set; }
        public string TariffName { get; set; }
        public string InsuredName { get; set; }
        public decimal PolisRealPayments { get; set; }
        public decimal Comission { get; set; }
        public decimal ComissionValue { get; set; }
        public string ActStatus { get; set; }
        public string BranchCode { get; set; }
        public string DirectionCode { get; set; }
        public int? BranchZone { get; set; }
        public string PersonType { get; set; }
        public string ProgramTypeName { get; set; }
        public int ChannelCode { get; set; }
        public decimal? IcsValue { get; set; }
        public decimal? Ics { get; set; }
        public string AgentIcsName { get; set; }
        public int? Franchise { get; set; }
        public string PolisTeritoryCode { get; set; }
        public decimal? PolisCoefficientK9 { get; set; }
        public decimal? PolisCoefficientK8 { get; set; }
        public int PolisDurationInDays { get; set; }
        public string VehicleType { get; set; }
        public int? AgentIcsChanelCode { get; set; }
        public string AgentIcsChanelName { get; set; }

    }
}
