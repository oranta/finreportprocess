﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using ExcelDataReader;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Repositories
{
    public class AgentsQuotaRepository : IAgentsQuotaRepository
    {
        private string basePortalUrl = @"http://portal.or.nt/Finance/";


        private async Task<DataTable> ProcessFileFromUrl(string targetUrl)
        {
            WebResponse response = null;
            DataTable resultDataTable;
            try
            {
                var request = CreateRequest(targetUrl);

                response = await request.GetResponseAsync();
                resultDataTable = SaveResponseFileToDataTable(response);

            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            finally
            {
                response?.Close();
            }

            return resultDataTable;
        }

        private static DataTable SaveResponseFileToDataTable(WebResponse response)
        {
            DataTable resultDataTable;
            using (Stream stream = response.GetResponseStream())
            {
                MemoryStream ms = new MemoryStream();

                stream?.CopyTo(ms);
                stream?.Close();

                resultDataTable = CreateDataTableFromStream(ms);
                ms.Close();
            }

            return resultDataTable;
        }

        private static DataTable CreateDataTableFromStream(Stream ms)
        {
            var excelDataReader = ExcelReaderFactory.CreateReader(ms);

            var result = excelDataReader.AsDataSet(new ExcelDataSetConfiguration
            {
                ConfigureDataTable = (table) => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true,
                    ReadHeaderRow = (rowReader) => { rowReader.Read(); }
                }

            });

            return result.Tables[0];
        }

        private static HttpWebRequest CreateRequest(string targetUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(targetUrl);
            var credentials = new NetworkCredential("SPConfigAcct", "SPConfigAcct", "ORANTA");
            request.Credentials = credentials;
            request.Timeout = 20000;
            request.AllowWriteStreamBuffering = false;
            return request;
        }


        public async Task<IEnumerable<ComissionPermissions>> GetComissionPermissionsesAsync(DateTime period)
        {
            // Grant ex.: new ComissionPermissions { PolisNumber = "АО-5181797", IcsComissionPermissions = 0, AgentComissionPermissions = 35 }

            string permissionUrl = basePortalUrl + $"GRANT/GRANT_{period.Month:00}-{period.Year}.xlsx";

            Console.WriteLine($"Get data from {permissionUrl}");

            DataTable permissionDataTable = await ProcessFileFromUrl(permissionUrl);


            var result = permissionDataTable.AsEnumerable().Select(x => new ComissionPermissions
            {
                PolisNumber = x[0].ToString(),
                IcsComissionPermissions = x.Field<double>(1),
                AgentComissionPermissions = x.Field<double>(2)
            });

            return result;
        }


        public async Task<IEnumerable<ControlledPolis>> GetControlledPolisesAsync(DateTime period)
        {
            // Control ex.: new ControlledPolis { PolisNumber = "АО-5181797", IcsControlledValue = 5, Comments = "партнер, квота, січень 2019" }

            string controlUrl = basePortalUrl + $"CONTROL/CONTROL_{period.Month:00}-{period.Year}.xlsx";

            Console.WriteLine($"Get data from {controlUrl}");

            DataTable permissionDataTable = await ProcessFileFromUrl(controlUrl);


            var result = permissionDataTable.AsEnumerable().Select(x => new ControlledPolis
            {
                PolisNumber = x[0].ToString(),
                IcsControlledValue = x.Field<double>(1),
                Comments = x.Field<string>(2)
            });

            return result;
        }


        public async Task<IEnumerable<ParkingPolis>> GeQuotaPolisesAsync(DateTime period)
        {
            // Quota ex.: new ParkingPolis { PolisNumber = "АО-5181797", Parking = "ні" }

            string quotaUrl = basePortalUrl + $"QUOTA/QUOTA_{period.Month:00}-{period.Year}.xlsx";

            Console.WriteLine($"Get data from {quotaUrl}");

            DataTable permissionDataTable =await ProcessFileFromUrl(quotaUrl);

            var result = permissionDataTable.AsEnumerable().Select(x => new ParkingPolis
            {
                PolisNumber = x[0].ToString(),
                Parking = x.Field<string>(1)
            });

            return result;

        }


        public async Task<IEnumerable<PolisQuotaIcs>> GetQuotaIcsesAsync(DateTime period)
        {
            // new PolisQuotaIcs { PolisNumber = "АО-5181797", IcsQuota = 13 }
            string newUrl = basePortalUrl + $"NEW/NEW_{period.Month:00}-{period.Year}.xlsx";

            Console.WriteLine($"Get data from {newUrl}") ;

            DataTable permissionDataTable =await ProcessFileFromUrl(newUrl);

            var result = permissionDataTable.AsEnumerable().Select(x => new PolisQuotaIcs
            {
                PolisNumber = x[0].ToString(),
                IcsQuota = x.Field<double>(1)
            });

            return result;
        }
    }
}