﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.DAL.Repositories
{
    public class RepositoryAgentsComission : IRepositoryAgentsComission
    {
        private readonly SqlConnection _db05Connection;

        public RepositoryAgentsComission()
        {
            _db05Connection = new SqlConnection("Data Source=hq01db02;Initial Catalog=ServiceBase;Integrated Security=True;Connect Timeout=0;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        }

        public async Task<IEnumerable<AgentComission>> GetAgentComissions(DateTime period)
        {
            var agentComissionDataTable = await GetAgentComissionTable(period);

            List<AgentComission> agentComissionList = new List<AgentComission>();

            foreach (DataRow agentComissionRow in agentComissionDataTable.Rows)
            {
                var t = new AgentComission();
                try
                {

                    t.AgentName = agentComissionRow.Field<string>("AgentName");
                    t.AgentFaceId = agentComissionRow.Field<string>("AgentFaceId");
                    t.ProgramCode = agentComissionRow.Field<string>("ProgramCode");
                    t.PolisNumber = agentComissionRow.Field<string>("PolisNumber");
                    t.TariffName = agentComissionRow.Field<string>("TariffName");
                    t.PolisDurationInDays = agentComissionRow.Field<int>("PolisDurationInDays");
                    t.PolisRealPayments = agentComissionRow.Field<decimal>("PolisRealPayments");
                    t.ComissionValue = agentComissionRow.Field<decimal>("ComissionValue");
                    t.Comission = agentComissionRow.Field<decimal>("Comission");
                    t.ActStatus = agentComissionRow.Field<string>("ActStatus");
                    t.BranchCode = agentComissionRow.Field<string>("BranchCode");
                    t.DirectionCode = agentComissionRow.Field<string>("DirectionCode");
                    t.PolisRegisteredDate = agentComissionRow.Field<DateTime>("PolisRegisteredDate");
                    t.ProgramTypeName = agentComissionRow.Field<string>("ProgramTypeName");
                    t.ChannelCode = int.Parse(agentComissionRow.Field<string>("ChannelCode"));
                    t.BusinessLineId = agentComissionRow.Field<int>("BusinessLineId");
                    t.BranchZone = agentComissionRow.Field<int?>("BranchZone");
                    t.PersonType = agentComissionRow.Field<string>("PersonType");
                    t.InsuredName = agentComissionRow.Field<string>("InsuredName");
                    t.IcsValue = agentComissionRow.Field<decimal?>("IcsValue");
                    t.Ics = agentComissionRow.Field<decimal?>("Ics");
                    t.AgentIcsName = agentComissionRow.Field<string>("AgentIcsName");
                    t.PolisCoefficientK8 = agentComissionRow.Field<decimal?>("PolisCoefficientK8");
                    t.PolisCoefficientK9 = agentComissionRow.Field<decimal?>("PolisCoefficientK9");
                    t.Franchise = agentComissionRow.Field<int?>("Franchise");
                    t.PolisTeritoryCode = agentComissionRow.Field<string>("PolisTeritoryCode");
                    t.VehicleType = agentComissionRow.Field<string>("VehicleType");
                    t.AgentIcsChanelCode = agentComissionRow.Field<int?>("AgentIcsChanelCode");
                    t.AgentIcsChanelName = agentComissionRow.Field<string>("AgentIcsChanelName");


                    agentComissionList.Add(t);
                }
                catch (InvalidCastException e)
                {
                    Console.WriteLine(e);

                    throw;
                }

            }

            return agentComissionList;
        }

        private async Task<DataTable> GetAgentComissionTable(DateTime period)
        {
            DataSet ds = new DataSet();

            //SqlCommand agentComissionComand = CreateAgentComissionsCommand(period);
            SqlCommand agentComissionComand = CreateAgentComissionCommandFromFile(period, "query.txt");
            agentComissionComand.Connection = _db05Connection;

            SqlDataAdapter adapter = new SqlDataAdapter(agentComissionComand);

            Console.WriteLine($"Get data from SQL");


            try
            {
                await Task.Run(() => adapter.Fill(ds, "AgentsComission"));
            }
            catch (SqlException sqlException)
            {
                Console.WriteLine(sqlException.Message);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            

            return ds.Tables["AgentsComission"];
        }

        private SqlCommand CreateAgentComissionsCommand(DateTime period)
        {
            string agentComissionQuery = $"SELECT * FROM [dbo].[FinReport] (@period)";
            SqlParameter periodParameter = new SqlParameter("@period", period);

            SqlCommand command = new SqlCommand(agentComissionQuery);
            command.CommandTimeout = 0;
            command.Parameters.Add(periodParameter);

            return command;
        }

        private SqlCommand CreateAgentComissionCommandFromFile(DateTime period, string filePath)
        {
            string file = Path.Combine(Environment.CurrentDirectory, filePath);
            string sqlExpression = String.Format(File.ReadAllText(file));

            SqlCommand command = new SqlCommand(sqlExpression);

            SqlParameter periodParam = new SqlParameter("@period", period);
            command.Parameters.Add(periodParam);
            command.CommandTimeout = 0;

            return command;

        }
    }
}
