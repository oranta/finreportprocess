﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.BL.Interfaces
{
    public interface IAgentActComissionService
    {
        Task<IEnumerable<AgentComission>> GetAgentComissionsAsync(DateTime period);
    }
}