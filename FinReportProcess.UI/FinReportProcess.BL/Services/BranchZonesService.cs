﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinReportProcess.BL.Interfaces;
using FinReportProcess.DAL;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.BL.Services
{
    public class BranchZonesService : IBranchZonesService
    {
        private readonly IBranchZoneRepository _branchZoneRepository;
        public Dictionary<string, string> BranchZoneAdapterDictionary { get; set; }
        public IEnumerable<BranchZone> BranchZones { get; private set; }

        public BranchZonesService(IBranchZoneRepository branchZoneRepository)
        {
            _branchZoneRepository = branchZoneRepository;
        }

        public async Task<IEnumerable<BranchZone>> GetAllBranchZonesAsync()
        {
            BranchZones = await _branchZoneRepository.GetBranchZonesAsync();
            BranchZoneAdapterDictionary = await _branchZoneRepository.GetBranchZoneAdapterDictionary();
            return BranchZones;
        }

        public int? GetZoneByBranchCode(string branchCode)
        {
            var branchZone = BranchZones.SingleOrDefault(bz => bz.BranchCode == branchCode.Substring(0, 4));
            return branchZone?.BranchZoneId;
        }

        public int? ChangeZone(string branchCode, int? zoneId)
        {
            return BranchZoneAdapterDictionary.ContainsKey(branchCode) ? GetZoneByBranchCode(BranchZoneAdapterDictionary[branchCode]) : zoneId;
        }
    }
}
