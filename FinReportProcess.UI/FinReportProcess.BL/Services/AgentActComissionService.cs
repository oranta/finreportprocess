﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinReportProcess.BL.Interfaces;
using FinReportProcess.DAL.Interfaces;
using FinReportProcess.DAL.Models;

namespace FinReportProcess.BL.Services
{
    public class AgentActComissionService : IAgentActComissionService
    {
        private readonly IRepositoryAgentsComission _repositoryAgentsComission;

        public AgentActComissionService(IRepositoryAgentsComission repositoryAgentsComission)
        {
            _repositoryAgentsComission = repositoryAgentsComission;
        }

        public async Task<IEnumerable<AgentComission>> GetAgentComissionsAsync(DateTime period)
        {

            return await _repositoryAgentsComission.GetAgentComissions(period);

        }
    }
}